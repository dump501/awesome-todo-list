//selectors
const todoInput = document.querySelector('.todo-input');
const todoButton = document.querySelector('.todo-button');
const todoList = document.querySelector('.todo-list');

//Events listeners
todoButton.addEventListener('click', addTodo);
todoList.addEventListener('click', deleteCheck);

//Functions
function addTodo(event)
{
    event.preventDefault();
    
    //create totdo div
    const todoDiv =  document.createElement('div');
    todoDiv.classList.add('todo');

    //create li
    const newTodo = document.createElement('li');
    newTodo.innerText = todoInput.value;
    newTodo.classList.add('todo-item');

    todoDiv.appendChild(newTodo);

    //check mark button
    const completedButton = document.createElement('button');
    completedButton.innerHTML = '<i class="fas fa-check"></i>';
    completedButton.classList.add("complete-btn");
    todoDiv.appendChild(completedButton);

    //check trash button
    const trashButton = document.createElement('button');
    trashButton.innerHTML = '<i class="fas fa-trash"></i>';
    trashButton.classList.add("trash-btn");
    todoDiv.appendChild(trashButton);

    //apprend to list
    todoList.appendChild(todoDiv);

    //erase the todo input value
    todoInput.value= "";
}

function deleteCheck(e)
{
    //console.log(e.target);

    const item = e.target;

    //delete the todo
    if(item.classList[0] === 'trash-btn')
    {
        //item.remove();
        const todo = item.parentElement;
        //Animation
        todo.classList.add("fall");
        todo.addEventListener('transitionend', function()
        {
            todo.remove();
        });
        //todo.remove();
    }

    //check mark
    if(item.classList[0] === 'complete-btn')
    { 
        //item.remove();
        const todo = item.parentElement;
        todo.classList.toggle('completed');
    } 
}